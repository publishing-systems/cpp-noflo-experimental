/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of cpp-noflo-experimental.
 *
 * cpp-noflo-experimental is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * cpp-noflo-experimental is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with cpp-noflo-experimental. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/JobFileLoader.cpp
 * @author Stephan Kreutzer
 * @since 2021-05-09
 */

#include "JobFileLoader.h"
#include "CppStAX/XMLInputFactory.h"

typedef std::unique_ptr<cppstax::XMLEvent> XMLEvent;

namespace noflo
{

JobFileLoader::JobFileLoader(std::istream& aStream):
  m_bLoaded(false)
{
    cppstax::XMLInputFactory aFactory;
    m_pReader = aFactory.createXMLEventReader(aStream);
}

JobFileLoader::~JobFileLoader()
{

}

int JobFileLoader::load()
{
    if (m_bLoaded == true)
    {
        throw new std::logic_error("noflo, JobFileLoader::load() called more than once.");
    }

    if (m_pReader == nullptr)
    {
        throw new std::logic_error("noflo, JobFileLoader::load() called while m_pReader == nullptr.");
    }

    while (m_pReader->hasNext() == true)
    {
        XMLEvent pEvent = m_pReader->nextEvent();

        if (pEvent->isStartElement() == true)
        {
            cppstax::StartElement& aStartElement = pEvent->asStartElement();
            const cppstax::QName& aName = aStartElement.getName();
            const std::string& strLocalPart = aName.getLocalPart();

            if (strLocalPart == "process")
            {
                const std::shared_ptr<cppstax::Attribute> pAttributeName(aStartElement.getAttributeByName(cppstax::QName("", "name", "")));

                if (pAttributeName == nullptr)
                {
                    throw new std::runtime_error("noflo, JobFileLoader: Element \"" + strLocalPart + "\" is missing its \"name\" attribute.");
                }

                const std::shared_ptr<cppstax::Attribute> pAttributeComponent(aStartElement.getAttributeByName(cppstax::QName("", "component", "")));

                if (pAttributeComponent == nullptr)
                {
                    throw new std::runtime_error("noflo, JobFileLoader: Element \"" + strLocalPart + "\" is missing its \"component\" attribute.");
                }

                if (m_aComponents.find(pAttributeName->getValue()) == m_aComponents.end())
                {
                    m_aComponents.insert(std::pair<std::string, std::string>(pAttributeName->getValue(), pAttributeComponent->getValue()));
                }
                else
                {
                    throw new std::runtime_error("noflo, JobFileLoader: Component \"" + pAttributeName->getValue() + "\" defined twice.");
                }
            }
            else if (strLocalPart == "connection")
            {
                HandleConnection();
            }
        }
    }

    m_pReader = nullptr;

    for (std::list<std::unique_ptr<JobStep>>::const_iterator iterConnection = m_aConnections.begin();
         iterConnection != m_aConnections.end();
         iterConnection++)
    {
        bool bFound = false;

        for (std::map<std::string, std::string>::const_iterator iterComponent = m_aComponents.begin();
            iterComponent != m_aComponents.end();
            iterComponent++)
        {
            if ((*iterConnection)->GetTgtProcess() == iterComponent->first)
            {
                // Ignoring/omitting the generic Component description from iterComponent->first.
                m_pJobGraph.push_back(std::pair<std::string, std::string>(iterComponent->second, (*iterConnection)->GetTgtProcess()));

                bFound = true;
                break;
            }
        }

        if (bFound != true)
        {
            throw new std::runtime_error("noflo, JobFileLoader: Process reference \"" + (*iterConnection)->GetTgtProcess() + "\" of a connection wasn't found within the list of processes.");
        }
    }

    m_bLoaded = true;

    return 0;
}

const std::list<std::pair<std::string, std::string>>& JobFileLoader::GetJobGraph()
{
    if (m_bLoaded != true)
    {
        throw new std::logic_error("noflo, JobFileLoader::GetJobGraph(): Called without calling JobFileLoader::GetJobGraph() first."); 
    }

    return m_pJobGraph;
}

int JobFileLoader::HandleConnection()
{
    std::unique_ptr<std::string> pSrcProcess;
    std::unique_ptr<std::string> pTgtProcess;
  
    while (m_pReader->hasNext() == true)
    {
        XMLEvent pEvent = m_pReader->nextEvent();

        if (pEvent->isStartElement() == true)
        {
            cppstax::StartElement& aStartElement = pEvent->asStartElement();
            const cppstax::QName& aName = aStartElement.getName();
            const std::string& strLocalPart = aName.getLocalPart();

            if (strLocalPart == "src")
            {
                const std::shared_ptr<cppstax::Attribute> pAttributeProcess(aStartElement.getAttributeByName(cppstax::QName("", "process", "")));

                if (pAttributeProcess == nullptr)
                {
                    throw new std::runtime_error("noflo, JobFileLoader: Element \"" + strLocalPart + "\" is missing its \"process\" attribute.");
                }

                if (pSrcProcess != nullptr)
                {
                    throw new std::runtime_error("noflo, JobFileLoader: Element \"connection\" contains more than one \"" + strLocalPart + "\" sub-element.");
                }

                // Optional.
                const std::shared_ptr<cppstax::Attribute> pAttributePort(aStartElement.getAttributeByName(cppstax::QName("", "port", "")));

                /** @todo No handling of src yet. */
            }
            else if (strLocalPart == "tgt")
            {
                const std::shared_ptr<cppstax::Attribute> pAttributeProcess(aStartElement.getAttributeByName(cppstax::QName("", "process", "")));

                if (pAttributeProcess == nullptr)
                {
                    throw new std::runtime_error("noflo, JobFileLoader: Element \"" + strLocalPart + "\" is missing its \"process\" attribute.");
                }

                if (pTgtProcess != nullptr)
                {
                    throw new std::runtime_error("noflo, JobFileLoader: Element \"connection\" contains more than one \"" + strLocalPart + "\" sub-element.");
                }

                // Optional.
                const std::shared_ptr<cppstax::Attribute> pAttributePort(aStartElement.getAttributeByName(cppstax::QName("", "port", "")));

                pTgtProcess.reset(new std::string(pAttributeProcess->getValue()));
            }
        }
        else if (pEvent->isEndElement() == true)
        {
            cppstax::EndElement& aEndElement = pEvent->asEndElement();
            const cppstax::QName& aName = aEndElement.getName();
            const std::string& strLocalPart = aName.getLocalPart();

            if (strLocalPart == "connection")
            {
                break;
            }
        }
    }

    if (pTgtProcess == nullptr)
    {
        throw new std::runtime_error("noflo, JobFileLoader: Element \"connection\" is missing required sub-element \"tgt\".");
    }

    std::unique_ptr<JobStep> pJobStep(new JobStep(pSrcProcess, pTgtProcess));
    m_aConnections.push_back(std::move(pJobStep));

    return 0;
}

}
