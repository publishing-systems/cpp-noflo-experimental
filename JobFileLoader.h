/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of cpp-noflo-experimental.
 *
 * cpp-noflo-experimental is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * cpp-noflo-experimental is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with cpp-noflo-experimental. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/JobFileLoader.h
 * @author Stephan Kreutzer
 * @since 2021-05-09
 */

#ifndef _NOFLO_JOBFILELOADER_H
#define _NOFLO_JOBFILELOADER_H

#include <istream>
#include <memory>
#include <map>
#include <string>
#include <list>
#include "CppStAX/XMLEventReader.h"
#include "JobStep.h"

typedef std::unique_ptr<cppstax::XMLEventReader> XMLEventReader;

namespace noflo
{

class JobFileLoader
{
public:
    JobFileLoader(std::istream& aStream);
    ~JobFileLoader();

public:
    int load();
    const std::list<std::pair<std::string, std::string>>& GetJobGraph();

protected:
    int HandleConnection();

protected:
    XMLEventReader m_pReader;
    bool m_bLoaded;
    /** @brief Map is Connection (caption) as key, Component name as value.
      * @details Allows for different Connection captions mapping to the same
      *     Component name, but same Connection caption mapping to same
      *     Component name would be duplicate, mapping to different Component
      *     names would be ambiguous. */
    std::map<std::string, std::string> m_aComponents;
    std::list<std::unique_ptr<JobStep>> m_aConnections;
    /** @brief Pair is Component name, caption. */
    std::list<std::pair<std::string, std::string>> m_pJobGraph;

};

}

#endif
