/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of cpp-noflo-experimental.
 *
 * cpp-noflo-experimental is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * cpp-noflo-experimental is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with cpp-noflo-experimental. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/JobStep.h
 * @author Stephan Kreutzer
 * @since 2021-05-09
 */

#ifndef _NOFLO_JOBSTEP_H
#define _NOFLO_JOBSTEP_H

#include <memory>
#include <string>

namespace noflo
{

class JobStep
{
public:
    JobStep(std::unique_ptr<std::string>& pSrcProcess, std::unique_ptr<std::string>& pTgtProcess);
    ~JobStep();

public:
    const std::unique_ptr<std::string>& GetSrcProcess();
    const std::string& GetTgtProcess();

protected:
    std::unique_ptr<std::string> m_pSrcProcess;
    std::unique_ptr<std::string> m_pTgtProcess;

};

}

#endif
