# Copyright (C) 2021  Stephan Kreutzer
#
# This file is part of cpp-noflo-experimental.
#
# cpp-noflo-experimental is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# cpp-noflo-experimental is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with cpp-noflo-experimental. If not, see <http://www.gnu.org/licenses/>.



.PHONY: all build clean



CFLAGS = -std=c++11 -Wall -Werror -Wextra -pedantic



all: ./CppStAX/cppstax.o build
build: noflo



./CppStAX/cppstax.o:
	$(MAKE) --directory=./CppStAX

noflo: noflo.cpp ./CppStAX/cppstax.o JobStep.o JobFileLoader.o MyComponent.o JavaComponent.o Component.o
	g++ noflo.cpp JobStep.o JobFileLoader.o MyComponent.o JavaComponent.o Component.o ./CppStAX/cppstax.o -o noflo $(CFLAGS)

MyComponent.o: Component.o MyComponent.h MyComponent.cpp
	g++ MyComponent.cpp -c $(CFLAGS)

JavaComponent.o: Component.o JavaComponent.h JavaComponent.cpp
	g++ JavaComponent.cpp -c $(CFLAGS)

Component.o: Component.h Component.cpp
	g++ Component.cpp -c $(CFLAGS)

JobStep.o: ./JobStep.h JobStep.cpp
	g++ JobStep.cpp -c $(CFLAGS)

JobFileLoader.o: ./CppStAX/XMLInputFactory.h ./CppStAX/XMLEventReader.h JobStep.o JobFileLoader.h JobFileLoader.cpp
	g++ JobFileLoader.cpp -c $(CFLAGS)

clean:
	rm -f ./noflo
	rm -f ./JobStep.o
	rm -f ./JobFileLoader.o
	rm -f ./MyComponent.o
	rm -f ./JavaComponent.o
	rm -f ./Component.o
