/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of cpp-noflo-experimental.
 *
 * cpp-noflo-experimental is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * cpp-noflo-experimental is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with cpp-noflo-experimental. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/JavaComponent.h
 * @author Stephan Kreutzer
 * @since 2021-05-1
 */

#ifndef _JAVACOMPONENT_H
#define _JAVACOMPONENT_H

#include "Component.h"
#include <memory>
#include <string>

/**
 * @class JavaComponent
 */
class JavaComponent : public noflo::Component
{
public:
    /** @brief Factory pattern. */
    static std::shared_ptr<JavaComponent> getComponent(const std::string& strDescription,
                                                       const std::string& strClasspath,
                                                       const std::string& strCapability);

protected:
    JavaComponent(const std::string& strDescription,
                  const std::string& strClasspath,
                  const std::string& strCapability);

public:
    ~JavaComponent();

    int process();

protected:
    std::string m_strClasspath;
    std::string m_strCapability;

};

#endif
