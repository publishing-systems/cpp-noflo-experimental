/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of cpp-noflo-experimental.
 *
 * cpp-noflo-experimental is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * cpp-noflo-experimental is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with cpp-noflo-experimental. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/Component.h
 * @author Stephan Kreutzer
 * @since 2021-05-01
 */

#ifndef _NOFLO_COMPONENT_H
#define _NOFLO_COMPONENT_H

#include <string>
#include <list>

namespace noflo
{

class Component
{
public:
    Component(const std::string& strDescription);
    virtual ~Component();

public:
    virtual int process() = 0;

    const std::string& GetDescription();

protected:
    std::string m_strDescription;
    /** @todo Parameter definitions should be noflo::InPort. */
    std::list<std::string> m_aInPorts;
    /** @todo Output definitions should be noflo::OutPort. */
    std::list<std::string> m_aOutPorts;

};

}

#endif
