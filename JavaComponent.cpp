/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of cpp-noflo-experimental.
 *
 * cpp-noflo-experimental is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * cpp-noflo-experimental is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with cpp-noflo-experimental. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/JavaComponent.cpp
 * @author Stephan Kreutzer
 * @since 2021-05-1
 */

#include "JavaComponent.h"
#include <sstream>
#include <cstdlib>

std::shared_ptr<JavaComponent> JavaComponent::getComponent(const std::string& strDescription,
                                                           const std::string& strClasspath,
                                                           const std::string& strCapability)
{
    return std::shared_ptr<JavaComponent>(new JavaComponent(strDescription,
                                                            strClasspath,
                                                            strCapability));
}

JavaComponent::JavaComponent(const std::string& strDescription,
                             const std::string& strClasspath,
                             const std::string& strCapability):
  noflo::Component(strDescription),
  m_strClasspath(strClasspath),
  m_strCapability(strCapability)
{

}

JavaComponent::~JavaComponent()
{

}

int JavaComponent::process()
{
    std::stringstream strCommand;
    strCommand << "java -cp " << m_strClasspath << " " << m_strCapability;

    std::system(strCommand.str().c_str());

    return 0;
}
