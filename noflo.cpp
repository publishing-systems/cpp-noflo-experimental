/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of cpp-noflo-experimental.
 *
 * cpp-noflo-experimental is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * cpp-noflo-experimental is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with cpp-noflo-experimental. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/noflo.cpp
 * @author Stephan Kreutzer
 * @since 2021-05-1
 */

#include "MyComponent.h"
#include "JavaComponent.h"
#include "JobFileLoader.h"
#include <memory>
#include <list>
#include <iostream>
#include <fstream>

std::unique_ptr<std::list<std::shared_ptr<noflo::Component>>> getComponents();

int main(int argc, char* argv[])
{
    std::cout << "cpp-noflo-experimental Copyright (C) 2021 Stephan Kreutzer\n"
              << "This program comes with ABSOLUTELY NO WARRANTY.\n"
              << "This is free software, and you are welcome to redistribute it\n"
              << "under certain conditions. See the GNU Affero General Public License 3\n"
              << "or any later version for details. Also, see the source code repository\n"
              << "https://gitlab.com/publishing-systems/cpp-noflo-experimental/ and\n"
              << "the project website https://publishing-systems.org.\n"
              << std::endl;

    if (argc < 2)
    {
        std::cout << "Usage:\n\n"
                  << "\tnoflo job-file\n"
                  << std::endl;

        return 1;
    }

    std::unique_ptr<noflo::JobFileLoader> pJobFileLoader;
    std::unique_ptr<std::ifstream> pStream = nullptr;

    try
    {
        pStream = std::unique_ptr<std::ifstream>(new std::ifstream);
        pStream->open(argv[1], std::ios::in | std::ios::binary);

        if (pStream->is_open() != true)
        {
            std::cout << "noflo: Couldn't open input file '" << argv[1] << "'.";
            return -1;
        }

        pJobFileLoader.reset(new noflo::JobFileLoader(*pStream));
        pJobFileLoader->load();

        pStream->close();
    }
    catch (std::exception* pException)
    {
        std::cout << "noflo: Exception: " << pException->what() << std::endl;

        if (pStream != nullptr)
        {
            if (pStream->is_open() == true)
            {
                pStream->close();
            }
        }

        return -1;
    }

    const std::list<std::pair<std::string, std::string>>& aJobGraph = pJobFileLoader->GetJobGraph();
    std::unique_ptr<std::list<std::shared_ptr<noflo::Component>>> pComponents(getComponents());

    for (std::list<std::pair<std::string, std::string>>::const_iterator iterJobNode = aJobGraph.begin();
         iterJobNode != aJobGraph.end();
         iterJobNode++)
    {
        bool bFound = false;

        for (std::list<std::shared_ptr<noflo::Component>>::const_iterator iterComponent = pComponents->begin();
             iterComponent != pComponents->end();
             iterComponent++)
        {
            if (iterJobNode->first == (*iterComponent)->GetDescription())
            {
                bFound = true;
                break;
            }
        }

        if (bFound != true)
        {
            throw new std::runtime_error("noflo: Job file wants to invoke component \"" + iterJobNode->first + "\", which is not available.");
        }
    }

    try
    {
        /** @todo Yeah, just lazy, dispatching with another loop for no good reason, just to avoid another data structure/list. */
        for (std::list<std::pair<std::string, std::string>>::const_iterator iterJobNode = aJobGraph.begin();
            iterJobNode != aJobGraph.end();
            iterJobNode++)
        {
            for (std::list<std::shared_ptr<noflo::Component>>::const_iterator iterComponent = pComponents->begin();
                iterComponent != pComponents->end();
                iterComponent++)
            {
                if (iterJobNode->first == (*iterComponent)->GetDescription())
                {
                    std::cout << "noflo: Calling node \"" << iterJobNode->second << "\" (component \"" << (*iterComponent)->GetDescription() << "\").\n";

                    (*iterComponent)->process();

                    break;
                }
            }
        }
    }
    catch (std::exception* pException)
    {
        std::cout << "noflo: Exception while running job, execution cancelled. Reason: " << pException->what() << std::endl;
        return -1;
    }

    return 0;
}

std::unique_ptr<std::list<std::shared_ptr<noflo::Component>>> getComponents()
{
    std::unique_ptr<std::list<std::shared_ptr<noflo::Component>>> pComponents(new std::list<std::shared_ptr<noflo::Component>>);

    /** @todo Discovery/loading based on metadata. */

    pComponents->push_back(MyComponent::getComponent());

    pComponents->push_back(JavaComponent::getComponent("XSLT Processor", "../digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/", "xml_xslt_transformator_1"));
    pComponents->push_back(JavaComponent::getComponent("File Picker", "../digital_publishing_workflow_tools/gui/file_picker/file_picker_1/", "file_picker_1"));

    return std::move(pComponents);
}
