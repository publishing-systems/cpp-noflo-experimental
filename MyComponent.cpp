/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of cpp-noflo-experimental.
 *
 * cpp-noflo-experimental is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * cpp-noflo-experimental is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with cpp-noflo-experimental. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/MyComponent.cpp
 * @author Stephan Kreutzer
 * @since 2021-05-1
 */

#include "MyComponent.h"
#include <iostream>

std::shared_ptr<MyComponent> MyComponent::m_pInstance;

std::shared_ptr<MyComponent> MyComponent::getComponent()
{
    if (MyComponent::m_pInstance == nullptr)
    {
        m_pInstance = std::shared_ptr<MyComponent>(new MyComponent);
    }

    return m_pInstance;
}

MyComponent::MyComponent():
  noflo::Component("MyComponent")
{
    m_aInPorts.push_back("in");
    m_aOutPorts.push_back("out");
    m_aOutPorts.push_back("error");
}

MyComponent::~MyComponent()
{
    m_pInstance = nullptr;
}

int MyComponent::process()
{
    std::cout << "hello, world!\n";
    return 0;
}
