/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of cpp-noflo-experimental.
 *
 * cpp-noflo-experimental is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * cpp-noflo-experimental is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with cpp-noflo-experimental. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/MyComponent.h
 * @author Stephan Kreutzer
 * @since 2021-05-1
 */

#ifndef _MYCOMPONENT_H
#define _MYCOMPONENT_H


#include "Component.h"
#include <memory>

/**
 * @class MyComponent
 */
class MyComponent : public noflo::Component
{
public:
    /** @brief Singleton pattern. */
    static std::shared_ptr<MyComponent> getComponent();

protected:
    MyComponent();

public:
    ~MyComponent();

    int process();

protected:
    static std::shared_ptr<MyComponent> m_pInstance;

};

#endif
